﻿using UnityEngine;

namespace WilliamGalvez
{
    public class Menu
    {
        public string nombre;
        public int precio;

		public Menu(string nombre, int precio)
		{	
			this.nombre = nombre;
			this.precio = precio;
		}

    }
}
