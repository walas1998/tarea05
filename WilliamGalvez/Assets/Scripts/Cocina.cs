﻿using UnityEngine;

namespace WilliamGalvez
{
    public class Cocina
    {
        public string nombre;
        public string tamaño;
        public int cantidad;

		public Cocina(string nombre, string tamaño, int cantidad)
		{	
			this.nombre = nombre;
			this.tamaño = tamaño;
			this.cantidad = cantidad;
		}

    }
}
