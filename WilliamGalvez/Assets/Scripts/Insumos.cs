﻿using UnityEngine;

namespace WilliamGalvez
{
    public class Insumos
    {
        public string nombre;
        public bool encender;

		public Insumos(string nombre, bool encender)
		{	
			this.nombre = nombre;
			this.encender = encender;
		}

    }
}
